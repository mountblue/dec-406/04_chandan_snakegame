'''
    Name:           snake.py
    Description:    A python script to implement a basic snake game using python libraries
    input:          integer values for game label and initial direction to move
    output:         a gui with given backgroud color and game content and player score
    usage:          python snake.py
                            <gamelabel:>
                            <initial direction:>

    example:        python htmlTableScript.py
                            2
                            RIGHT

    author:         Chandan Shaw

'''

#importing required libraries
import random
import sys

#importing pygame module to implement the game structure
import pygame

#class to implement the contents and attributes
class snake_body:
    #initializing snake attributes
    def __init__(self,direction):
        self.position = [60,50]
        self.body = [[60,50],[60 - 1,50 - 1],[60 - 2,50 - 2]]
        self.direction = direction
        self.width = 612
        self.hieght = 408

    #function to get the snake body
    def body(self):
        return self.body

    #function to keep track snake movement
    def snake_movement(self,foodpos):
        if self.direction == "RIGHT":
            self.position[0] += 1
        if self.direction == "LEFT":
            self.position[0] -= 1
        if self.direction == "UP":
            self.positon[1] += 1
        if self.direction == "DOWN":
            self.postion[1] -= 1
        self.body.eat_food(foodpos)

    #function to change snake direction
    def snake_direction_change(self,move):
        if move == "RIGHT" and self.direction == "LEFT":
            self.direction = "RIGHT"
        if move == "LEFT" and self.direction == "RIGHT":
            self.direction = "LEFT"
        if move == "UP" and self.direction == "DOWN":
            self.direction = "UP"
        if move == "DOWN" and self.direction == "UP":
            self.direction = "DOWN"

    #function to increase the snake body if food position collides
    def eat_food(self,foodpos):
        self.body.insert(0,self.foodpos)
        if self.position == foodpos:
            return
        else:
            self.body.pop()

    #function to check the collision of head with body
    def check_collision(self):
        if self.position in self.body:
            return True
        elif self.postion[0] == self.width or self.position[1] == self.hieght or self.position[0] == 0 or self.position[1] == 0:
            return True
        else:
            return False

#function to generate food at random position
def generate_food(foodExist):
    if not foodExist:
        foodPosition = [random.randrange(1,612),random.randrange(1,408)]
        foodExist = True
    return foodPosition, foodExist

#function to keep track the event from user sides
def pygame_event(snake):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                snake.snake_direction_change('RIGHT')
            if event.key == pygame.K_LEFT:
                snake.snake_direction_change('LEFT')
            if event.key == pygame.K_UP:
                snake.snake_direction_change('UP')
            if event.key == pygame.K_DOWN:
                snake.snake_direction_change('DOWN')
    return snake

#main function to take input and initialize game
def main():
    label = input("Enter game label 1-2-3: ")
    starting_direction = input("Enter the direction to start: ")
    window = pygame.display.set_mode((612,408))
    pygame.display.set_caption("")
    window.fill(pygame.Color(225,225,225))
    frame = pygame.time.Clock()
    if label == 1:
        pygame.display.flip(18)
    elif label == 2:
        pygame.display.flip(24)
    elif label == 3:
        pygame.display.flip(30)


    snake = snake_body(starting_direction)

    while True:
        snake = pygame_event(snake)
        print(snake)

    food_position,foodExist = generate_food(foodExist=True)
    no_of_count = 0

    if snake.snake_movement(foodpos):
        no_of_count += 1
        foodExist = False

    for position in snake.body():
        pygame.draw.rect(window,pygame.Color(0,225,0),pygame.Rect(position[0],position[1],10,10))
    pygame.draw.rect(window,pygame.Color(0,225,0),pygame.Rect(food_position[0],food_position[1],10,10)
    if snake.check_collision():
        print("GAME OVER")
        pygame.exit()
        sys.exit()

    pygame.display.set_caption("Score: "+str(no_of_count))
    pygame.display.flip()

#to call the main function 
if __name__ == '__main__':
    main()
